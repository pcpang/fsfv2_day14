/**
 * Created by pohchye on 15/7/2016.
 */
// Create the Express app
var express=require("express");
var app=express();

// Path module
var path=require("path");

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': false}));
app.use(bodyParser.json());

// Load mysql module
var mysql=require("mysql");
// Create mysql connection pool
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "employees",
    connectionLimit: 4
});

var Employee = function (first_name, last_name, gender, birth_date, hire_date) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.gender = gender;
    this.birth_date = birth_date;
    this.hire_date = hire_date;
};

// Method 2
Employee.prototype.save = function (successCallback, errorCallback) {
    console.info("Saving Employee");
    var self = this;
    pool.getConnection(function (err, connection) {
        if (err) {
            return errorCallback(err);
        }
        connection.query(INSERTSQL,
            [self.first_name, self.last_name, self.gender, self.birth_date, self.hire_date],
            function (err, result) {
                connection.release();
                if (err) {
                    return errorCallback(err);
                }
                successCallback(result);
            }
        );
    });
};
// Can use to set the default value for properties too
// End of Day 14

// *** Middleware ***

// Filter using 2 tables
const INSERTSQL = "insert into employees (first_name, last_name, gender, birth_date, " +
    "hire_date) values (?,?,?,?,?)";
app.post("/api/employee/save", function(req, res) {
    console.info("SQL: " + INSERTSQL);
    // Perform Queries
    var gender = req.body.gender == 'male' ? 'M' : 'F'; //enum fields
    var birthDate = req.body.birthday.substring(0, req.body.birthday.indexOf('T'));
    var hireDate = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T'));
    var employee = new Employee(req.body.firstname, req.body.lastname, gender, birthDate, hireDate);
    console.info("New Employee: " + employee.first_name);
    // Method 1
    // employee.save(res);
    // Method 2
    employee.save(function success(result) {
        console.info("Success");
        res.status(202).json({url: "/api/employee/query" + result.insertId});
    }, function error(err) {
        res.status(500).end();
        console.info("Error occurred: " + err);
    });
});

app.get("/api/employee/query/:emp_no", function (req, res) {
    pool.getConnection(function (err, connection) {
        if (err) {
            res.status(400).send(JSON.stringify(err));
            return;
        }
        connection.query("select * from employees where emp_no = ?",
            [req.params.emp_no],
            function (err, results) {
                connection.release();
                if (err) {
                    res.status(400).send(JSON.stringify(err));
                    return;
                }
                if (results.length) {
                    console.info("Employee Retrieve: " + results[0].first_name);
                    res.json(results[0]);
                } else {
                    res.status(404).end("emp_no " + req.params.emp_no + " is not found");
                }
            });
    });
});

// Serve public files
app.use(express.static(__dirname + "/public"));
// - For bower_components that is on the root directory instead of public
// app.use("/bower_components",express.static(path.join(__dirname, "bower_components")));
// app.use(express.static(path.join(__dirname,"public")));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3014
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});