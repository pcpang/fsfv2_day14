/**
 * Created by pohchye on 18/7/2016.
 */
(function () {

    angular.module("RegApp")
        .controller("RegCtrl", RegCtrl)
        .controller("ListCtrl", ListCtrl);

    // Inject RegCtrl controller with http and PersistSvc services
    RegCtrl.$inject = ["$http", "PersistSvc"];

    // RefCtrl controller
    function RegCtrl($http, PersistSvc) {
        var vm = this;
        vm.employee = PersistSvc.getEmployeeObject();
        vm.employeeID = "";
        vm.result = "";
        vm.status = {
            message: "",
            code: 0
        };
        
        vm.register = function () {
            console.info("Register Service");
            PersistSvc.register(vm.employee)
                .then(function (result) {
                    console.info("Data URL: " + result.url);
                    vm.status.message = "Success in saving employee";
                    vm.status.code = 202;
                    vm.employee = PersistSvc.getEmployeeObject();
                })
                .catch(function (error) {
                    console.info("Error: " + error);
                    vm.status.message = "Failure in saving employee";
                    vm.status.code = 400;
                });
        };
        
        vm.query = function () {
            console.info("Query Service: " + vm.employeeID);
            PersistSvc.query(vm.employeeID)
                .then(function (result) {
                    console.info("Employee: " + result.first_name);
                    vm.status.message = "Success in query employee";
                    vm.status.code = 202;
                    vm.result = result;
                })
                .catch(function (error) {
                    console.info("Error: " + error);
                    vm.status.message = "Failure in query employee";
                    vm.status.code = 400;
                });
        };
    }

    // angular
    //     .module("RegApp")
    //     .controller("ListCtrl", ListCtrl);
    // ListCtrl.$inject = ["PersistSvc", "$scope"];
    // function ListCtrl (PersistSvc, $scope){
    //     var vm = this;
    //     vm.employees = PersistSvc.employees;
    //     $scope.employees = PersistSvc.employees;
    // }

    // Inject ListCtrl controller with http and PersistSvc services
    ListCtrl.$inject = ["PersistSvc"];

    // Controller ListCtrl
    function ListCtrl(PersistSvc) {
        var self = this;
        self.employees = PersistSvc.employees;
    }
    
})();