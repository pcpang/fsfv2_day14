/**
 * Created by pohchye on 18/7/2016.
 */
(function () {

    angular.module("RegApp")
        .service("PersistSvc", PersistSvc);

    // PersistSvc service
    PersistSvc.$inject = ["$http", "$q"];
    function PersistSvc($http, $q) {
        var defer = $q.defer();
        var service = this;
        service.employees = [];

        this.register = function (employee) {
            $http.post("/api/employee/save", employee)
                .then(function (result) {
                    service.employees.push(employee);
                    defer.resolve(result.data);
                })
                .catch(function (error) {
                    defer.reject(error.status);
                });
            return (defer.promise);
        };

        this.query = function (employee_ID) {
            $http.get("/api/employee/query/" + employee_ID)
                .then(function (result) {
                    console.info("Service Result: " + result.data.first_name);
                    defer.resolve(result.data);
                })
                .catch(function (error) {
                    defer.reject(error.status);
                });
            return (defer.promise);
        };

        this.getEmployeeObject = function () {
            var employee = {};
            employee.firstname = "";
            employee.lastname = "";
            employee.gender = "";
            employee.birthday = "";
            employee.hiredate = "";
            return employee;
        };

    }
    
})();